//创建一个录音对象
var recorder;

/**
* 音频初始化
* 
* obj的格式：
* {
	url: xxxx,
	enableUpload: false,
	uploadSucc: function(data){},
	
  }
*/
function InitAudio(obj){
	//判断浏览器是否支持调用麦克风、摄像头等设备
	if (!navigator.mediaDevices) {
		alert('您的浏览器不支持获取用户设备');
		return;
	}
	if (!window.MediaRecorder) {
		alert('您的浏览器不支持录音');
		return;
	}
	
	//获取音频流
	navigator.mediaDevices.getUserMedia({
		audio: true
	}).then(stream => {
		//new一个录音对象
		recorder = new MediaRecorder(stream);
		//设置回调
		recorder.ondataavailable = function(e){
			console.log("录音结束后数据处理！");
			//是否开启上传
			if(obj.enableUpload){
				//进行上传
				updateAudio(e.data, obj);
			}
		}
		
		recorder.onstop = function(){
			console.log("录音结束");
		}
		
	}, error => {
		alert('出错，请确保已允许浏览器获取录音权限');
	});
}


/**
 * ajax上传录音
 */
function updateAudio(audio, obj){
	var formData = new FormData();
	formData.set("file", audio);
	$.ajax({
		type: "POST",
		contentType: false,
		processData: false,
		url: obj.url,
		data: formData,
		success: function(data){
			if(obj.uploadSucc){
				obj.uploadSucc(data);
			}
		}
	});
}

/**
 * 开始录音
 */
function startRecoder(){
	recorder.start();
}

/**
 * 结束录音
 */
function stopRecoder(){
	recorder.stop();
}